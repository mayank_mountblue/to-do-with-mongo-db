$("document").ready(function() {
	var $value;
	var idOfList;
	//loading data from database and creating list
		$.ajax({
		type:"get",
		url:"/todos",
		success: function(tasks) {
			$.each(tasks, function(i, task) {
			idOfList = task._id;
				createList(task.name);
			});
		},
		error: function() {
			alert("something went wrong");
		}
		});


	$("input").keyup(function(ev) {
		$value = ($(this).val());
		if (ev.which == 13) {
			if ($value == "") {
				alert("kindly enter the value");
			} else {
				$.ajax({
                    type: 'POST',
                    url: '/todos',
                    data: {
						name: $value,
						
                    },
                    datatype: "application/json",
                    success: function(newTasking) {
						var text = newTasking.name;
						 idOfList = newTasking._id;
						createList(text);
                    },
                    error: function() {
                        alert("something went wrong");
                    }
                });
				
				
                $(this).val("");
            }
				

			}
		});
		//creating for checkbox
			function mycheckbox() {
		var $checkbutton = $('<input >').attr({
			class: 'checkbutton',
			type: "checkbox"
		});
		return $checkbutton;
	}

	//creating close button
	function closeButton() {
		var closeButton = $("<span></span")
			.append("\u00D7");
		$(closeButton).attr({
			class: "close"
		});
		return closeButton;
	}
	//creating  list
	function createList($value) {

		$("ul").prepend($('<li>').attr({
			class: "list-group-item",
			id: idOfList
		}));
		$("#" + idOfList).append(mycheckbox());
		$("#" + idOfList).append($value);
		$("#" + idOfList).append(
			closeButton());
		

	}

	
//function when clicking the close button
	$("ul").on("click", "li .close",
		function() {
					var currentId = $(this).parent()[0].id;
        $($(this).parent()[0]).remove();
        $.ajax({
            type: 'DELETE',
            url: '/todos/' + currentId,
            data: {
                id: currentId
            }
        });
			$(this).parent().css("display",
				"none");
		});
		//function for checklist
	$("ul").click(function(ev) {
		if (ev.target.className ===
			"checkbutton") {
			if ($(ev.target).prop('checked') ==
				true) {
				$(ev.target).parent().css(
					"textDecoration",
					"line-through");
			} else {
				$(ev.target).parent().css(
					"textDecoration", "none");
			}

		}
	});


})