var express = require("express");
var app = express();
var mongoose = require("mongoose");
var bp = require("body-parser");
var path = require("path");
var schema = mongoose.Schema;
var todoSchema = new schema({
    done: { type: String, default: false},
    name: { type: String,default: false }
});
app.get("/",function(req,res){
 res.sendFile(path.join(__dirname, 'index.html'));
});

var list = mongoose.model("todoDatas", todoSchema);
mongoose.connect("mongodb://localhost:27017/todoData");
//app.use(express.static());
app.use(express.static(__dirname));
app.use(bp.urlencoded({ extended: false }))

app.get("/todos", (req, res) => {
    list.find({}, (err, data) => {
        if (err) throw err;
        else {
            res.json(data);
        }
    });
});
app.post("/todos", (req, res) => {
    var data = {
        name: req.body.name,
        done: req.body.done
    };
    var obj = new list(data);
    obj.save((err, result) => {
        if (err) throw err;
        else
            // res.redirect("/");
            res.json(result);
            
    });
});
app.delete("/todos/:id", (req, res) => {
    list.remove({ _id: req.params.id }, (err, result) => {
        if (err) throw err;
        res.send("deleted");
    });
});
app.put("/todos/:id", (req, res) => {
    list.update({ id: req.params._id }, { name: req.body.name, done: req.body.done }, (err, result) => {
        if (err) throw err;
    });
});
app.listen(3000);